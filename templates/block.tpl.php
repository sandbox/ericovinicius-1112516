<?php if (!empty($block->subject)) { ?>

  <div id="block-<?php print $block->module .'-'. $block->delta ?>" class="<?php print $block_classes . ' ' . $block_zebra; ?> collapsible">
    <div class="block-inner">
  
      <?php if (!empty($block->subject)): ?>
        <div class="handlediv"><h3 class="title block-title"><?php print $block->subject; ?></h3></div>
      <?php endif; ?>
        <div class="inside">
        <div class="content">
          <?php print $block->content; ?>
        </div>
        <?php print $edit_links; ?>
      </div>
  </div> <!-- /block-inner -->
  </div> <!-- /block -->
  
  <?php } else { ?>
  
  <div id="block-<?php print $block->module .'-'. $block->delta ?>" class="<?php print $block_classes . ' ' . $block_zebra; ?>">
    <div class="block-inner">
  
      <div class="content">
        <?php print $block->content; ?>
      </div>
  
      <?php print $edit_links; ?>
  
    </div> <!-- /block-inner -->
  </div> <!-- /block -->

<?php } ?>