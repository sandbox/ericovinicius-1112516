$(document).ready(function() {

	
	/**
	 *  Quick tab cookies
	 **/
	
	// dashboard quicktab element
	var dashboard_qt = $('#block-aim_learning-dashboard .quicktabs_tabs li a');
	
	// get stored cookie for dashboard quicktabs
	var dashboard_qt_index = $.cookie("dashboard_qt_index");
	
	// tricker click event for tab at cookie index
	$('#block-aim_learning-dashboard .quicktabs_tabs li a:eq('+dashboard_qt_index+')').trigger('click');
	
	// set dashboard cookie when changing quicktabs
	$(dashboard_qt).click( function() {
		var index = $(dashboard_qt).index(this);
		$.cookie("dashboard_qt_index", index, { path: '/' });
	});



	
	/**
	 *  Collapsible block cookies
	 *  - collapsed cookies are named by block id.
	 **/
	 	
	//
	// Get stored cookies and add collapsed classes
	//	
	$.each($('.block.collapsible'), function() {
		var id = $(this).attr('id');
		var collapsed = $.cookie(id);
		
		// if collapsed is set, add collapsed class
		if (collapsed > 0) {
			$(this).addClass('collapsed');
		}
	});	


	//
	// Set collapsed/expanded cookie when handlediv is clicked
	//	
	$('div.collapsible .handlediv').click( function() {
		var block = $(this).parents('.block');
		var id = $(block).attr('id');
		
		// if block is already collapsed, then remove 'collapsed'
		if ($(block).hasClass('collapsed')) {
			var collapsed = 0;
		}	else {
			var collapsed = 1;
		}
		
		// set collapsed cookie
		$.cookie(id, collapsed, { path: '/' });
	});


			
});